<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
        <form action="/login" method="POST">
            @csrf
            <h1>Buat Account Baru!</h1>
            <h3>Sign Up Form</h3>

            <label for="first">First name</label> <br><br>
            <input type="text" id="first" name="firstname">
            <br><br>
            <label for="last">Last name</label> <br><br>
            <input type="text" id="last" name="lastname"> <br><br>

            <label>Gender:</label> <br><br>
            <input type="radio" name="gender" value="0">Male <br>
            <input type="radio" name="gender" value="1">Female <br>
            <input type="radio" name="gender" value="2">Other <br><br>

            <label>Nationality:</label> <br><br>
            <select>
                <option value="ind">Indonesian</option>
                <option value="usa">USA</option>
                <option value="french">French</option>
                <option value="china">China</option>
            </select>
            <br><br>

            <label>Language Spoken:</label> <br><br>
            <input type="checkbox" name="bahasa" value="1">Bahasa Indonesia <br>
            <input type="checkbox" name="bahasa" value="2">English <br>
            <input type="checkbox" name="bahasa" value="3">Other <br><br>

            <label for="bio">Bio:</label><br><br>
            <textarea rows="9" cols="25" id="bio"></textarea><br><br>

            <input type="submit" value="login">
        </form>
    </body>
</html>
