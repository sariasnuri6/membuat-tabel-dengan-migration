<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
        
        <h1>Selamat Datang {{$first}} {{$last}}</h1>
        <p>Terima kasih telah bergabung di Sanberbook, Social media kita bersama!</p>
    </body>
</html>
